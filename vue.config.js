const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: [
    'sweetalert2',
    'vuetify'
  ]
})
